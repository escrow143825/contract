// SPDX-License-Identifier: MIT
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/interfaces/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

abstract contract IERC20Extend is IERC20 {
    function decimals() external view virtual returns (uint);
}

contract EscrowV3 is OwnableUpgradeable {
    event Deposit(address indexed user, uint balance, uint timestamp);
    event Withdraw(address indexed user, uint balance, uint timestamp);
    event CreateUser(address indexed user, bool isMerchant);
    event Stake(address indexed user, uint amount, uint timestamp);
    event Unstake(address indexed user, uint amount, uint timestamp);

    struct User {
        uint balance;
        uint timestamp;
        bool isMerchant;
        uint staked;
    }

    mapping(address => User) private users;
    uint public userCounter;
    IERC20Extend public token;

    uint public feePercentage;

    struct Statistic {
        uint totalBalance;
        uint totalDeposited;
        uint totalStaked;
    }

    Statistic statistic;

    function initialize(address tokenAddress) public initializer {
        __Ownable_init(msg.sender);
        token = IERC20Extend(tokenAddress);
        feePercentage = 100;
    }

    function setFee(uint fee) public onlyOwner {
        feePercentage = fee;
    }

    function isUserExist(address walletAddress) public view returns (bool) {
        return users[walletAddress].timestamp != 0;
    }

    function createUser(
        address walletAddress,
        bool isMerchant
    ) public onlyOwner {
        require(!isUserExist(walletAddress), "user exist");
        users[walletAddress] = User(0, block.timestamp, isMerchant, 0);
        userCounter = userCounter + 1;
        emit CreateUser(walletAddress, isMerchant);
    }

    function deposit(uint amount) public {
        require(isUserExist(msg.sender), "user not exist");
        require(amount > 0, "greater than 0");
        require(amount <= token.balanceOf(msg.sender), "insufficient");

        (uint fee, uint netAmount) = getAmountFee(amount);
        require(
            token.transferFrom(msg.sender, address(this), netAmount),
            "deposit failed"
        );
        require(token.transferFrom(msg.sender, owner(), fee), "deposit failed");

        User storage user = users[msg.sender];
        user.balance = user.balance + netAmount;
        user.timestamp = block.timestamp;

        statistic.totalBalance = statistic.totalBalance + netAmount;
        statistic.totalDeposited = statistic.totalDeposited + netAmount;

        emit Deposit(msg.sender, netAmount, block.timestamp);
    }

    function withdraw(address to, uint amount) public {
        require(isUserExist(to), "user not exist");
        require(amount > 0, "greater than 0");

        User storage user = users[to];
        require(amount <= user.balance, "insufficient");

        (uint fee, uint netAmount) = getAmountFee(amount);

        require(token.transfer(to, netAmount), "withdraw failed");
        require(token.transfer(owner(), fee), "withdraw failed");

        user.balance = user.balance - amount;
        user.timestamp = block.timestamp;

        statistic.totalBalance = statistic.totalBalance - amount;
        statistic.totalDeposited = statistic.totalDeposited - amount;

        emit Withdraw(to, netAmount, block.timestamp);
    }

    function stake(uint amount) public {
        require(isUserExist(msg.sender), "user not exist");
        require(amount > 0, "greater than 0");

        User storage user = users[msg.sender];
        require(amount <= user.balance, "insufficient");

        (uint fee, uint netAmount) = getAmountFee(amount);
        require(token.transfer(owner(), fee), "stake failed");

        user.balance = user.balance - amount;
        user.staked = user.staked + netAmount;
        user.timestamp = block.timestamp;

        statistic.totalBalance = statistic.totalBalance - amount;
        statistic.totalStaked = statistic.totalStaked + netAmount;

        emit Stake(msg.sender, amount, block.timestamp);
    }

    function unstake(uint amount) public {
        require(isUserExist(msg.sender), "user not exist");
        require(amount > 0, "greater than 0");
        User storage user = users[msg.sender];
        require(amount <= user.staked, "insufficient");

        (uint fee, uint netAmount) = getAmountFee(amount);
        require(token.transfer(owner(), fee), "unstake failed");

        user.staked = user.staked - netAmount;
        user.balance = user.balance + netAmount;
        user.timestamp = block.timestamp;

        statistic.totalBalance = statistic.totalBalance + netAmount;
        statistic.totalStaked = statistic.totalStaked - amount;

        emit Unstake(msg.sender, amount, block.timestamp);
    }

    function getTotalBalance() public view returns (uint) {
        return statistic.totalBalance;
    }

    function getTotalStaked() public view returns (uint) {
        return statistic.totalStaked;
    }

    function getUser(address _address) public view returns (User memory) {
        return users[_address];
    }

    function getAmountFee(uint amount) public view returns (uint, uint) {
        uint fee = (amount * feePercentage) / 10000;
        uint netAmount = amount - fee;
        return (fee, netAmount);
    }
}
