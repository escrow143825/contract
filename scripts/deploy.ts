import { task } from "hardhat/config";

task("deploy-token-v3").setAction(async (args, { ethers, upgrades }) => {
  const factory = await ethers.getContractFactory("TokenV3");
  const token = await upgrades.deployProxy(factory, ["TokenV3", "TOK"], {
    initializer: "initialize",
  });
  await token.waitForDeployment();
  console.log("TokenV3 deployed to:", await token.getAddress());
  //0x2305D47e99d982415a2e351E5a19C70C8e0c5B1D
});

task("deploy-escrow-v3").setAction(async (args, { ethers, upgrades }) => {
  const factory = await ethers.getContractFactory("EscrowV3");
  const contract = await upgrades.deployProxy(
    factory,
    ["0x2305D47e99d982415a2e351E5a19C70C8e0c5B1D"],
    {
      initializer: "initialize",
    }
  );
  await contract.waitForDeployment();
  console.log("EscrowV3 deployed to:", await contract.getAddress());
  //0x534D076F7A7809d7A602476B283752026c3F6b78
});
