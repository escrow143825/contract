import { HardhatEthersSigner } from "@nomicfoundation/hardhat-ethers/signers";
import { ethers, upgrades } from "hardhat";
import { EscrowV3, TokenV2, TokenV3 } from "../typechain-types";
import {
  Signature,
  TypedDataDomain,
  parseEther,
  verifyTypedData,
} from "ethers";

describe("Escrow", function () {
  let account0: HardhatEthersSigner;
  let account1: HardhatEthersSigner;
  let account2: HardhatEthersSigner;
  let account3: HardhatEthersSigner;

  let token: TokenV3;
  let tokenAddress: string;
  let tokenName = "TokenV2";
  let tokenSymbol = "TOK";

  let contract: EscrowV3;
  let contractAddress: string;

  const deployTokenAndContract = async () => {
    [account0, account1, account2, account3] = await ethers.getSigners();

    // //
    // const Token = await ethers.getContractFactory("TokenV2");
    // token = (await upgrades.deployProxy(Token, [tokenName, tokenSymbol], {
    //   initializer: "initialize",
    // })) as unknown as TokenV2;
    // await token.waitForDeployment();
    // tokenAddress = await token.getAddress();
    // console.log("TokenV2 deployed to:", await token.getAddress());

    //
    const TokenV3 = await ethers.getContractFactory("TokenV3");
    token = (await upgrades.deployProxy(TokenV3, [tokenName, tokenSymbol], {
      initializer: "initialize",
    })) as unknown as TokenV3;
    await token.waitForDeployment();
    tokenAddress = await token.getAddress();
    console.log("TokenV3 deployed to:", await token.getAddress());

    //
    const Escrow = await ethers.getContractFactory("EscrowV3");
    contract = (await upgrades.deployProxy(Escrow, [tokenAddress], {
      initializer: "initialize",
    })) as unknown as EscrowV3;
    await contract.waitForDeployment();
    contractAddress = await contract.getAddress();
    console.log("EscrowV3 deployed to:", await contract.getAddress());
  };

  it("should deploy", async function () {
    await deployTokenAndContract();

    await token.connect(account0).transfer(account1, parseEther("100"));
    await token.connect(account0).transfer(account1, parseEther("100"));
  });

  it("should deposit", async function () {
    const chainId = (await ethers.provider.getNetwork()).chainId;
    const owner = account1;
    const spender = account2;
    const value = parseEther("50");
    const deadline = Math.floor(Date.now() / 1000) + 60 * 20;
    const nonce = await token.nonces(await owner.getAddress());
    const domain: TypedDataDomain = {
      name: await token.name(),
      version: "1",
      chainId,
      verifyingContract: await token.getAddress(),
    };
    const types = {
      Permit: [
        { name: "owner", type: "address" },
        { name: "spender", type: "address" },
        { name: "value", type: "uint256" },
        { name: "nonce", type: "uint256" },
        { name: "deadline", type: "uint256" },
      ],
    };

    const values = {
      owner: await owner.getAddress(),
      spender: await spender.getAddress(),
      value,
      nonce: nonce,
      deadline,
    };

    const signature = await owner.signTypedData(domain, types, values);
    const { v, r, s } = Signature.from(signature);

    const tx = await token
      .connect(spender)
      .permit(
        await owner.getAddress(),
        await spender.getAddress(),
        value,
        deadline,
        v,
        r,
        s
      );

    const receipt = await tx.wait();
    const d = await contract.connect(spender).deposit(value);

    console.log(await token.allowance(owner, spender));
  });
});
